# deluge-web.service
Quick and dirty systemd unit file for deluge-web, because for some reason the deluge-web Debian package maintainer decided not to include one. (Related: http://dev.deluge-torrent.org/ticket/2034 )

I wrote these instructions from memory months after I set this up on my seedbox. If there's anything wrong, I'm sure you're smart enough to figure it out.

After installing deluge, deluge-console, deluge-web, and deluge-webui, clone the repository and move the files to their proper homes:

```bash
git clone https://github.com/resuni/deluge-web.service.git
mv deluge-web.service /lib/systemd/system/
mv start-deluge-web.sh /var/lib/deluged/deluge-web/
mv stop-deluge-web.sh /var/lib/deluged/deluge-web/
```

The scripts need to run as the "debian-deluged" user, so change the ownership/permissions of the files like so:

```bash
chown debian-deluged:debian-deluged /var/lib/deluged/deluge-web/start-deluge-web.sh
chown debian-deluged:debian-deluged /var/lib/deluged/deluge-web/stop-deluge-web.sh
chmod 744 /var/lib/deluged/deluge-web/start-deluge-web.sh
chmod 744 /var/lib/deluged/deluge-web/stop-deluge-web.sh
```

Finally, reload systemd's daemons:

```bash
systemctl daemon-reload
```

Now you should be able use systemctl controls to start/stop and enable/disable deluge-web. 

On a final note, if deluge-web terminates unexpectedly and you can't get it to start again using systemctl, you probably need to manually delete the pid file that's created at /var/lib/deluged/deluge-web/deluge-web.pid. I could probably write some logic into the start script to fix this, but I'm too lazy. 
